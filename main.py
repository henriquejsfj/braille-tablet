import PyPDF2
import braille.braille as braille
import os
from word2number.w2n import word_to_num as w2n
from tika import parser
import signal
import time
import threading
from googletrans import Translator


# texto = "Oi gente! Esse é um exemplo mostrando que dá para criar um Braille Tablet."
# braille.textToBraille(texto)
# # braille.textToSpeech(texto)
# print(braille.imageToText("sample2.png"))

# print(braille.speechToText())

# print("End")




#
# # creating a pdf reader object
# pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
#
# # printing number of pages in pdf file
# print(pdfReader.numPages)
#
# for page in range(pdfReader.numPages):
#     # creating a page object
#     pageObj = pdfReader.getPage(page)
#
#     # extracting text from page
#     print(pageObj.extractText())
#     # print_braille(pageObj.extractText())
#     braille.textToBraille(pageObj.extractText())
#     input("Enter para continuar")
#
# # closing the pdf file object
# pdfFileObj.close()
def lost(signum, frame):
    while (True):
        braille.textToSpeech("Estou aqui! Aperte enter quando me encontrar.")

class FiveSec(threading.Thread):
    def stop(self):
        self.aux = False
    def run(self, *args):
        self.aux = True
        while self.aux:
            braille.textToSpeech("Estou aqui! Aperte enter quando me encontrar.")

        # os.kill(os.getpid(), signal.SIGINT)

def main():
    translator = Translator()
    pdfs = []
    arqs = os.listdir()
    for arq in arqs:
        if arq[-3:] == "pdf":
            pdfs.append(arq[:-4])

    braille.textToSpeech("Olá, bem-vindo ao braile tablet! O que deseja?")

    while(True):
        try: cmd = braille.speechToText().lower()

        except: cmd = ""
        print("CMD:", cmd)
        if cmd.find("listar") != -1:
            braille.textToSpeech("Ok, listando arquivos...")
            ind = 1
            for arq in pdfs:
                braille.textToSpeech("Arquivo " + str(ind) + ". " + arq)
                ind += 1
            braille.textToSpeech("Agora basta dizer. abrir, e o número do arquivo.")
        elif cmd.find("abrir") != -1:
            try:
                print(translator.translate(cmd[cmd.find("abrir")+5:], dest='en').text)
                arq = pdfs[w2n(translator.translate(cmd[cmd.find("abrir")+5:], dest='en').text)-1]
                braille.textToSpeech("Abrindo, " + arq + " ponto pê dê efe.")
            except:
                braille.textToSpeech("Não entendi o número.")
                continue

            raw = parser.from_file(arq + '.pdf')
            output = open(arq + ".txt", 'w')
            braille.textToSpeech("Escrevendo em braile.")
            braille.textToBraille(raw['content'], file=output)
            output.close()
            os.system("gedit " + arq + ".txt")
            braille.textToSpeech("Pronto!")

        elif cmd.find("aonde") != -1:
            braille.textToSpeech("Estou aqui!")
            t = FiveSec()
            t.daemon = True
            t.start()
            input('::> ')
            t.stop()
        elif cmd.find("desligar") != -1:
            braille.textToSpeech("Desligando...")
            break
        else:
            braille.textToSpeech("Fale algum comando válido")




if __name__ == "__main__":
    # calling the main function
    main()

